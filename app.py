from logging.config import dictConfig

from flask import Flask

from routes.home import home_api
from routes.rest import rest_api
from routes.update import update_api
from routes.view import view_api

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s :: %(levelname)s] %(filename)s - %(funcName)s (ln %(lineno)d) : %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})
app = Flask(__name__)


app.register_blueprint(home_api)
app.register_blueprint(update_api, url_prefix='/update')
app.register_blueprint(view_api, url_prefix='/view')
app.register_blueprint(rest_api, url_prefix='/rest')

if __name__ == '__main__':
    app.run()
