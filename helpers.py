import functools
import uuid
import warnings

from services.database.nodes.resource import Resource

reserved_fields = ["name", "docid", "course", "pdfpath", "date"]


def filterform(data: dict):
    ret = {}
    for key in data:
        if key not in reserved_fields:
            ret[key] = data[key]
    return ret


def genguuid(prefix: str = "", suffix: str = "") -> str:
    """
    generates random guuid
    :param prefix: string part that has to preceed the guid
    :param suffix: string part that has to follow
    :return: uuid with suffix
    """
    return prefix + str(uuid.uuid4()) + suffix


def deprecated(func):
    """This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emitted
    when the function is used."""

    @functools.wraps(func)
    def new_func(*args, **kwargs):
        warnings.simplefilter('always', DeprecationWarning)  # turn off filter
        warnings.warn("Call to deprecated function {}.".format(func.__name__),
                      category=DeprecationWarning,
                      stacklevel=2)
        warnings.simplefilter('default', DeprecationWarning)  # reset filter
        return func(*args, **kwargs)

    return new_func


def process_files(files):
    for file in files:
        guuid = genguuid()
        ext = file.filename[file.filename.index(".") + 1:]
        new_filename = "data/{0}.{1}".format(guuid, ext)
        file.save(new_filename)
        yield Resource(resid=guuid, name=file.filename, location=new_filename, restype=ext, tags={})
