from flask import Blueprint

from services.database import db

rest_api = Blueprint('rest_api', __name__)


@rest_api.route('/rel/resdoc/<resid>/<docid>')
def is_related_res_doc(resid, docid):
    return 'CONNECTED' if resid in [res.resid for res in db.get_resources_by_doc(docid)] else 'DISJUNCT'