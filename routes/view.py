from flask import Blueprint, render_template, send_file, request

from services.database import db

view_api = Blueprint('view_api', __name__)


@view_api.route('/res/<resid>')
def viewres(resid):
    file = db.get_resource(resid)
    return send_file(file.location)


@view_api.route('/doc/<docid>')
def viewdoc(docid):
    nomenu = 'nomenu' in request.args.keys()
    return render_template('views/viewdoc.jinja2', document=db.get_doc(docid),
                           resources=db.get_resources_by_doc(docid), nomenu=nomenu)


@view_api.route('/course/<courseid>')
def viewcourse(courseid):
    return render_template('views/course_overview.jinja2', course=db.get_course(courseid),
                           documents=db.get_docs_from_course(courseid))
