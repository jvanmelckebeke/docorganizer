from flask import Blueprint, request, redirect, url_for, render_template

from services.database import db

update_api = Blueprint('update_api', __name__)


@update_api.route('/doc/<docid>', methods=["GET", "POST"])
def update_doc(docid):
    if docid == "new":
        ndoc = db.create_doc()
        suffix = "?closeAfterCompletion" if request.args.get("closeAfterCompletion") is not None else ""
        return redirect("/update/doc/" + ndoc.docid + suffix)

    if request.method == "GET":
        return render_template("views/editdoc.jinja2", doc=db.get_doc(docid), courses=db.get_courses(),
                               resources=db.get_resources())
    elif request.method == "POST":
        form = request.form
        tags = {form.getlist("tags[]")[i]: form.getlist("tagvalues[]")[i] for i in range(len(form.getlist("tags[]")))}
        db.update_doc(docid, name=form.get("name"), tags=tags, date=form.get("date"))
        for resource in form.getlist("resources"):
            db.relate_resource_document(resource, docid)
        if form.get("courseid") is not None:
            db.update_relate_doc_course(docid, form.get("courseid"))
        if form.get("closeAfterCompletion"):
            return redirect("/dashboard?closeAfterCompletion")
    return redirect("/")


@update_api.route('/res/<resid>', methods=["GET", "POST"])
def updateres(resid):
    if request.method == "GET":
        return render_template("views/editresource.jinja2", resource=db.get_resource(resid), documents=db.get_docs())
    elif request.method == "POST":
        form = request.form
        tags = {form.getlist("tags[]")[i]: form.getlist("tagvalues[]")[i] for i in range(len(form.getlist("tags[]")))}
        db.update_resource(resid, name=form.get("name"), tags=tags, date=form.get("date"))
        return redirect("/")


@update_api.route('/course/<courseid>', methods=["GET", "POST"])
def courseoverview(courseid):
    if courseid == "new":
        ndoc = db.create_course()
        suffix = "?closeAfterCompletion" if request.args.get("closeAfterCompletion") is not None else ""
        return redirect("/update/course/" + ndoc.courseid + suffix)

    if request.method == "POST":
        form = request.form
        tags = {form.getlist("tags[]")[i]: form.getlist("tagvalues[]")[i] for i in range(len(form.getlist("tags[]")))}
        db.update_course(courseid, name=form.get("name"), tags=tags)
        if form.get("closeAfterCompletion"):
            return redirect("/dashboard?closeAfterCompletion")
        else:
            return redirect("/")
    else:
        return render_template('views/editcourse.jinja2', course=db.get_course(courseid))
