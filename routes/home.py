from flask import Blueprint, render_template, request, redirect, url_for

from services.database import db

home_api = Blueprint('home_api', __name__)


@home_api.route('/')
@home_api.route('/dashboard')
def home():
    db.run_routine_clean_empty()
    return render_template('views/dashboard.jinja2', unsorted=db.unsorted_resources(), courses=db.get_courses(),
                           documents=db.get_docs(), resources=db.get_resources())


@home_api.route('/upload/', methods=["GET", "POST"])
def upload_bulk():
    if request.method == "GET":
        return render_template("views/upload.jinja2")
    files = request.files.getlist("files[]")
    db.insert_bulk_res(files)
    return redirect("/")
