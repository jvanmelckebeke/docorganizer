import json

from py2neo import Node


class Resource(object):
    def __init__(self, resid, name, location, restype, tags=None):
        self.resid = resid
        self.name = name
        self.location = location
        self.restype = restype
        self.tags = tags if tags is not None else {}

    def to_node(self) -> Node:
        node = Node("Resource")
        dic = self.to_dict()
        for key in dic:
            node[key] = dic[key]
        return node

    @classmethod
    def from_node(cls, node):
        return Resource(name=node["name"], location=node["location"], resid=node["resid"], restype=node["restype"],
                        tags=node["tags"])

    def to_dict(self):
        dic = {
            "resid": self.resid,
            "name": self.name,
            "location": self.location,
            "restype": self.restype
        }
        dic.update(self.tags)
        return dic

    def __repr__(self) -> str:
        return json.dumps(self.to_dict(), indent=2)
