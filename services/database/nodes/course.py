import json

from py2neo import Node

from helpers import genguuid


class Course(object):
    def __init__(self, courseid, name, tags=None):
        self.courseid = courseid if courseid is not None else genguuid()
        self.name = name if name is not None else ''
        self.tags = tags if tags is not None else {}

    def to_node(self):
        node = Node("Course")
        dic = self.to_dict()
        for key in dic:
            node[key] = dic[key]
        return node

    @classmethod
    def from_node(cls, node):
        return Course(node["courseid"], node["name"], node["tags"])

    def to_dict(self):
        dic = {
            "name": self.name,
            "courseid": self.courseid
        }
        dic.update(self.tags)
        return dic

    def __repr__(self) -> str:
        return json.dumps(self.to_dict(), indent=2)
