import json

from py2neo import Node

import helpers
from helpers import genguuid


class Document(object):
    def __init__(self, name, docid=None, date=None, tags=None):
        """
        default initializer
        :param name: name of the document
        :param docid: document identifier
        :param tags: additional tags
        :return:
        """
        self.name = name if name is not None else ''
        self.date = date
        self.docid = docid if docid is not None else genguuid()
        self.tags = tags if tags is not None else dict()

    def add_tag(self, tagname: str, tagvalue):
        self.tags[tagname] = tagvalue

    def to_dict(self) -> dict:
        """
        converts current document to a dict object
        :return: dict containing the Document
        """
        output_dict = {'name': self.name,
                       'docid': self.docid,
                       'date': self.date}
        output_dict.update({tag: self.tags[tag] for tag in self.tags.keys()})
        return output_dict

    def to_node(self) -> Node:
        node = Node("Document")
        dic = self.to_dict()
        for key in dic:
            node[key] = dic[key]
        return node

    @staticmethod
    def from_node(d: Node):
        return Document(name=d["name"],
                        docid=d["docid"],
                        date=d["date"] if "date" in d else None,
                        tags=helpers.filterform(d))

    def __repr__(self) -> str:
        return json.dumps(self.to_node(), indent=2)
