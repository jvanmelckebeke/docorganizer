from py2neo import Relationship


class DocumentOf(Relationship):
    pass


class RevisionOf(Relationship):
    pass


class ResourceOf(Relationship):
    pass
