import logging

from flask.logging import default_handler
from py2neo import Graph, NodeMatcher, RelationshipMatcher, walk

from helpers import process_files
from services.database.nodes.course import Course
from services.database.nodes.document import Document
from services.database.nodes.resource import Resource
from services.database.relations import ResourceOf, DocumentOf

logger = logging.getLogger()
logger.addHandler(default_handler)

GRAPH_URI = "bolt://localhost:7687"
NEO_AUTH = "neo4j", "graph"
RESERVED_FIELDS = ["name", "docid", "courseid", "resid", "location", "date"]


class _Database(object):
    def __init__(self):
        self.graph = Graph(GRAPH_URI, auth=NEO_AUTH)

    def _start_transaction(self):
        return self.graph.begin()

    def unsorted_resources(self):
        nodematcher = NodeMatcher(self.graph)
        rm = RelationshipMatcher(self.graph)
        resnodes = nodematcher.match("Resource")
        rels = rm.match(r_type=ResourceOf)
        for node in resnodes:
            dangling = True
            for rel in rels:
                if node["resid"] == next(walk(rel))["resid"]:
                    dangling = False
                    break
            if dangling:
                yield Resource.from_node(node)

    def get_doc(self, docid):
        nodematcher = NodeMatcher(self.graph)
        result = nodematcher.match("Document").where("_.docid = '" + docid + "'")
        return Document.from_node(result.first())

    def insert_bulk_res(self, files):
        for res in process_files(files):
            self.insert(res.to_node())

    def update_doc(self, docid: str, name: str = None, date=None, tags: dict = None):
        tags = tags if tags is not None else {}
        newnode = self._delete_removed_tags(docid, tags, "Document", "docid")
        if tags is not None:
            newnode.update(tags)
        if name is not None:
            newnode["name"] = name
        if date is not None:
            newnode["date"] = date
        self.graph.push(newnode)

    def get_resource(self, resid):
        nm = NodeMatcher(self.graph)
        node = nm.match("Resource").where(resid=resid).first()
        return Resource.from_node(node)

    def get_resource_by_name(self, name):
        nm = NodeMatcher(self.graph)
        node = nm.match("Resource").where(name=name).first()
        return Resource.from_node(node)

    def get_course(self, courseid):
        nm = NodeMatcher(self.graph)
        node = nm.match("Course").where(courseid=courseid).first()
        return Course.from_node(node) if node is not None else None

    def get_course_by_name(self, name):
        nm = NodeMatcher(self.graph)
        node = nm.match("Course").where(name=name).first()
        return Course.from_node(node)

    def insert(self, node):
        tx = self._start_transaction()
        tx.create(node)
        tx.commit()

    def get_courses(self):
        nm = NodeMatcher(self.graph)
        nodes = nm.match("Course")
        return [Course.from_node(node) for node in nodes]

    def create_course(self):
        course = Course(None, None)
        self.insert(course.to_node())
        return course

    def update_course(self, courseid, name, tags):
        newnode = self._delete_removed_tags(courseid, tags, "Course", "courseid")
        if tags is not None:
            newnode.update(tags)
        if name is not None or name != "":
            newnode["name"] = name
        newnode = Course.from_node(newnode)
        if self.get_course(courseid) is not None:
            tx = self._start_transaction()
            tx.merge(newnode.to_node(), primary_label="Course", primary_key='courseid')
            tx.commit()
        else:
            self.graph.push(newnode.to_node())

    def update_relate_doc_course(self, docid, courseid):
        if self.get_rels_course_doc(docid) is not None:
            tx = self._start_transaction()
            for rel in self.get_rels_course_doc(docid):
                tx.separate(rel)
            tx.commit()
        nm = NodeMatcher(graph=self.graph)
        docnode = nm.match("Document").where(docid=docid).first()
        coursenode = nm.match("Course").where(courseid=courseid).first()
        relation = DocumentOf(docnode, coursenode)
        self.insert(relation)

    def _delete_removed_tags(self, idval, tags, category, idname):
        nm = NodeMatcher(graph=self.graph)
        node = nm.match(category).where("_.{0} = '{1}'".format(idname, idval)).first()
        todelete = []
        for tag in node:
            if tag not in tags.keys() and tag not in RESERVED_FIELDS:
                todelete.append(tag)

        for tag in todelete:
            del node[tag]
        return node

    def update_resource(self, resid, name, tags, date):
        newnode = self._delete_removed_tags(resid, tags, "Resource", "resid")
        if tags is not None:
            newnode.update(tags)
        if name is not None:
            newnode["name"] = name
        if date is not None:
            newnode["date"] = date
        self.graph.push(newnode)

    def create_doc(self):
        doc = Document(None)
        self.insert(doc.to_node())
        return doc

    def get_docs(self):
        nm = NodeMatcher(self.graph)
        nodes = nm.match("Document")
        return [Document.from_node(node) for node in nodes]

    def get_docs_from_course(self, courseid):
        rm = RelationshipMatcher(self.graph)
        result = rm.match(r_type=DocumentOf).where("b.courseid = '" + courseid + "'")
        return [Document.from_node(next(walk(res))) for res in result] if result is not None else None

    def get_rels_course_doc(self, docid):
        rm = RelationshipMatcher(self.graph)
        result = rm.match(r_type=DocumentOf).where("a.docid = '" + docid + "'")
        ret = [DocumentOf.cast(res) for res in result]
        return ret

    def get_resources_by_doc(self, docid):
        rm = RelationshipMatcher(self.graph)
        result = rm.match(r_type=ResourceOf).where("b.docid = '" + docid + "'")
        ret = [Resource.from_node(next(walk(res))) for res in result]
        return ret

    def relate_resource_document(self, resid, docid):
        nm = NodeMatcher(graph=self.graph)
        resnode = nm.match("Resource").where(resid=resid).first()
        docnode = nm.match("Document").where(docid=docid).first()
        relation = ResourceOf(resnode, docnode)
        self.insert(relation)

    def get_resources(self):
        nm = NodeMatcher(graph=self.graph)
        ret = nm.match("Resource")
        return [Resource.from_node(node) for node in ret]

    def run_routine_clean_empty(self):
        logger.info("running routine clean job")
        nm = NodeMatcher(graph=self.graph)
        emptynodes = nm.match().where(name="")
        for node in emptynodes:
            logger.info("removing node with id %s", node["name"])
            tx = self._start_transaction()
            tx.delete(node)
            tx.commit()
        logger.info("all empty nodes removed")
